const config = require('./config');
const Cache = require('./cache');
const TeamspeakAsync = require('teamspeak-async');
const Winston = require('winston');

const logger = new Winston.Logger({
  transports: [
    new Winston.transports.Console(),
    new Winston.transports.File({
      name: 'error-file',
      filename: 'errors.log',
      level: 'error',
    }),
  ],
});

class InstaTs3 {
  constructor(db) {
    this.cache = new Cache();
    this.db = db;
    this.config = config.ts3;
    this.connect();
  }

  connect() {
    try {
      this.client = new TeamspeakAsync.TeamSpeakClient(this.config);
      this.client.on('cliententerview', data => this.onClientEnterView(data));
      this.client.on('clientleftview', data => this.onClientLeftView(data));
      this.client.on('clientmoved', data => this.onClientMoved(data));
      this.client.on('channelmoved', data => this.onChannelMoved(data));
      this.client.on('channeledited', data => this.onChannelEdited(data));
      this.client.on('channeldeleted', data => this.onChannelDeleted(data));
      this.client.on('channelcreated', data => this.onChannelCreated(data));
      this.client.on('error', err => this.onError(err));
      this.client.socket.on('connect', () => this.onConnect());
      this.client.socket.on('close', err => this.onClose(err));
      this.client.socket.on('error', err => this.onError(err));
    } catch (error) {
      logger.error(error);
    }
  }


  async qServerInfo() {
    try {
      const x = await this.client.send('serverinfo');
      if ('error' in x) {
        throw new Error(x.msg);
      }
      return x;
    } catch (error) {
      logger.error(error);
      return null;
    }
  }

  async qCreateChannel(template) {
    try {
      if (!template) {
        throw new Error('Empty param for query.');
      }
      const x = await this.client.send('channelcreate', template);
      if ('error' in x) {
        throw new Error(x.msg);
      }
      return x;
    } catch (error) {
      if (error.toString().match(/channel name is already in use/g)) {
        return -2;
      }
      logger.error(error);
      return null;
    }
  }

  async qChannelDetails(cid, force = false) {
    try {
      if (!cid) {
        throw new Error('Empty param for query.');
      }
      let x = await this.cache.getAsync('channelInfo:'.concat(cid));
      if (x === null || force) {
        x = await this.client.send('channelinfo', { cid });
        if ('error' in x) {
          throw new Error(x.msg);
        }
        if (parseInt(x.pid, 10) === 0) { // ignorowanie subkanalow
          this.cache.set(
            'channelInfo:'.concat(cid), x, 'PX', config.cache.channelInfo);
        }
      }
      return x;
    } catch (error) {
      logger.error(error);
      return null;
    }
  }

  async qRemoveChannel(cid) {
    try {
      if (!cid) {
        throw new Error('Empty param for query.');
      }
      const x = await this.client.send('channeldelete', { cid, force: 0 });
      if ('error' in x) {
        throw new Error(x.msg);
      } // z cache sie usunie no problemo
      return x;
    } catch (error) {
      if (!error.toString().match(/ok/g)) {
        logger.error(error);
      }
      return null;
    }
  }

  async qClientDetails(clid) {
    try {
      if (!clid) {
        throw new Error('Empty param for query.');
      }
      let x = await this.cache.getAsync('clientInfo:'.concat(clid));
      if (x === null) {
        x = await this.client.send('clientinfo', { clid });
        if ('error' in x) {
          throw new Error(x.msg);
        }
        this.cache.set(
          'clientInfo:'.concat(clid), x, 'PX', config.cache.clientInfo);
      }
      return x;
    } catch (error) {
      logger.error(error);
      return null;
    }
  }

  async qChannelList(force = false) {
    try {
      let x = await this.cache.getAsync('channelList');
      if (x === null || force) {
        x = await this.client.send('channellist', '-topic');
        if ('error' in x) {
          throw new Error(x.msg);
        }
        if (x.length === undefined) {
          x = [x];
        }
        this.cache.set(
          'channelList', x, 'PX', config.cache.channelList);
      }
      return x;
    } catch (error) {
      if (!error.toString().match(/empty result set/g)) {
        logger.error(error);
      }
      return [];
    }
  }

  async qChannelGroupClientList(cid) {
    try {
      if (!cid) {
        throw new Error('Empty param for query.');
      }
      let x = await this.cache.getAsync('channelGroupClientList:'.concat(cid));
      if (x === null) {
        x = await this.client.send('channelgroupclientlist', { cid });
        if ('error' in x) {
          throw new Error(x.msg);
        }
        if (x.length === undefined) {
          x = [x];
        }
        this.cache.set(
          'channelGroupClientList:'.concat(cid), x, 'PX', config.cache.channelGroupClientList);
      }
      return x;
    } catch (error) {
      if (!error.toString().match(/empty result set/g)) {
        logger.error(error);
      }
      return [];
    }
  }

  async qClientDbInfo(cldbid) {
    try {
      if (!cldbid) {
        throw new Error('Empty param for query.');
      }
      let x = await this.cache.getAsync('clientDbInfo:'.concat(cldbid));
      if (x === null) {
        x = await this.client.send('clientdbinfo', { cldbid });
        if ('error' in x) {
          throw new Error(x.msg);
        }
        this.cache.set(
          'clientDbInfo:'.concat(cldbid), x, 'PX', config.cache.clientDbInfo);
      }
      return x;
    } catch (error) {
      logger.error(error);
      return null;
    }
  }

  async qClientKick(clid, reasonid, reasonmsg) {
    try {
      if (!(clid && reasonid && reasonmsg)) {
        throw new Error('Empty param for query.');
      }
      const x = await this.client.send('clientkick', { clid, reasonid, reasonmsg });
      if ('error' in x) {
        throw new Error(x.msg);
      }
      return x;
    } catch (error) {
      if (!error.toString().match(/ok/g)) {
        logger.error(error);
      }
      return null;
    }
  }

  async qChannelFind(pattern) {
    try {
      if (!pattern) {
        throw new Error('Empty param for query.');
      }
      let x = await this.client.send('channelfind', { pattern });
      if ('error' in x) {
        throw new Error(x.msg);
      }
      if (x.length === undefined) {
        x = [x];
      }
      return x;
    } catch (error) {
      if (!error.toString().match(/empty result set/g)) {
        logger.error(error);
      }
      return [];
    }
  }

  async qSetClientChannelGroup(cid, cldbid, cgid) {
    try {
      if (!(cid && cgid && cldbid)) {
        throw new Error('Empty param for query.');
      }
      const x = await this.client.send('setclientchannelgroup', { cid, cgid, cldbid });
      if ('error' in x) {
        throw new Error(x.msg);
      }
      this.cache.client.del('channelGroupClientList:'.concat(cid));
      return x;
    } catch (error) {
      if (!error.toString().match(/ok/g)) {
        logger.error(error);
      }
      return null;
    }
  }

  async qClientList() {
    try {
      let x = await this.cache.getAsync('clientList');
      if (x === null) {
        x = await this.client.send('clientlist');
        if ('error' in x) {
          throw new Error(x.msg);
        }
        if (x.length === undefined) {
          x = [x];
        }
        x = x.filter(cl => parseInt(cl.client_type, 10) === 0);
        this.cache.set(
          'clientList', x, 'PX', config.cache.clientList);
      }
      return x;
    } catch (error) {
      if (!error.toString().match(/empty result set/g)) {
        logger.error(error);
      }
      return [];
    }
  }

  async qChannelGroupList() {
    try {
      let x = await this.cache.getAsync('channelGroupList');
      if (x === null) {
        x = await this.client.send('channelgrouplist');
        if ('error' in x) {
          throw new Error(x.msg);
        }
        if (x.length === undefined) {
          x = [x];
        }
        x = x.filter(
          group => (group.cgid > 10),
        );
        this.cache.set(
          'channelGroupList', x, 'PX', config.cache.channelGroupList);
      }
      return x;
    } catch (error) {
      logger.error(error);
      return [];
    }
  }

  async qServerGroupList() {
    try {
      let x = await this.cache.getAsync('serverGroupList');
      if (x === null) {
        x = await this.client.send('servergrouplist');
        if ('error' in x) {
          throw new Error(x.msg);
        }
        if (x.length === undefined) {
          x = [x];
        }
        this.cache.set(
          'serverGroupList', x, 'PX', config.cache.serverGroupList);
      }
      return x;
    } catch (error) {
      logger.error(error);
      return [];
    }
  }

  async qClientPoke(clid, msg) {
    try {
      if (!(clid && msg)) {
        throw new Error('Empty param for query.');
      }
      const x = await this.client.send('clientpoke', { clid, msg });
      if ('error' in x) {
        throw new Error(x.msg);
      }
      return x;
    } catch (error) {
      if (!error.toString().match(/ok/g)) {
        logger.error(error);
      }
      return null;
    }
  }

  async qChannelAddPerm(cid, permid, permvalue) {
    try {
      if (!(cid && permid && permvalue)) {
        throw new Error('Empty param for query.');
      }
      const x = await this.client.send('channeladdperm', { cid, permid, permvalue });
      if ('error' in x) {
        throw new Error(x.msg);
      }
      return x;
    } catch (error) {
      if (!error.toString().match(/empty result set/g) &&
        !error.toString().match(/ok/g)
      ) {
        logger.error(error);
      }
      return null;
    }
  }

  async qServerGroupDelClient(cldbid, sgid) {
    try {
      if (!(cldbid && sgid)) {
        throw new Error('Empty param for query.');
      }
      const x = await this.client.send('servergroupdelclient', { cldbid, sgid });
      if ('error' in x) {
        throw new Error(x.msg);
      }
      return x;
    } catch (error) {
      if (!error.toString().match(/empty result set/g) &&
        !error.toString().match(/ok/g)
      ) {
        logger.error(error);
      }
      return null;
    }
  }

  async qServerGroupAddClient(cldbid, sgid) {
    try {
      if (!(cldbid && sgid)) {
        throw new Error('Empty param for query.');
      }
      const x = await this.client.send('servergroupaddclient', { cldbid, sgid });
      if ('error' in x) {
        throw new Error(x.msg);
      }
      return x;
    } catch (error) {
      if (!error.toString().match(/empty result set/g) &&
        !error.toString().match(/ok/g)
      ) {
        logger.error(error);
      }
      return null;
    }
  }

  /*
    name: getChanneList
    description: Lista kanałów używanych przez system
    return: [] of { cid, clients, name } || []
  */
  async getChannelList(force = false) {
    try {
      let channelList = await this.qChannelList(force);

      channelList = channelList.filter(
        channel => (channel.channel_topic === config.ts3.create.topic),
      );

      channelList = channelList.map(
        channel => ({
          cid: channel.cid,
          clients: channel.total_clients,
          name: channel.channel_name,
        }),
      );

      return channelList;
    } catch (error) {
      if (!error.toString().match(/empty result set/g)) {
        logger.error(error);
      }
      return [];
    }
  }

  /*
    name: getDefaultChannelID
    description: Znajdź id kanału domyślnego na serwerze
    return: cid || null
  */
  async getDefaultChannelID() {
    try {
      let channelList = await this.qChannelList();

      channelList = channelList.filter(
        channel => !(channel.channel_topic === config.ts3.create.topic),
      );

      const channelsDetails = await this.getChannelDetailsOfAll(channelList);

      for (let i = 0; i < channelsDetails.length; i += 1) {
        if (parseInt(channelsDetails[i].channel_flag_default, 10) === 1) {
          const cid = channelList[i].cid;
          return cid;
        }
      }
    } catch (error) {
      logger.error(error);
    }

    return null;
  }

  /*
    name: getActiveChannelGroups
    description: Zwraca listę użytkowników na kanale wraz z ich grupami
    return: array of { name, group_name } || null
  */
  async getActiveChannelGroups(cid) {
    try {
      const clientList = await this.qChannelGroupClientList(cid);
      const channelGroupList = await this.qChannelGroupList(cid);
      const clientsDbInfo = await this.getClientDbInfoOfAll(clientList);
      const response = [];

      clientsDbInfo.forEach((clientDbInfo, index) => {
        if (clientDbInfo !== null) {
          const o = {
            name: clientDbInfo.client_nickname,
          };

          const channelGroup = channelGroupList.find(
            group => (group.cgid === clientList[index].cgid),
          );

          if (channelGroup !== undefined) {
            o.group_name = channelGroup.name;
          }

          response.push(o);
        }
      });

      return response;
    } catch (error) {
      logger.error(error);
      return null;
    }
  }

  /*
    name: getClientListOnChannel
    description: Zwraca listę użytkowników na kanale
    return: array of { clid, cldbid, name, time, county } || []
  */
  async getClientListOnChannel(cid, cl = null) {
    try {
      if (cl === null) {
        cl = await this.qClientList(); // eslint-disable-line
      }

      let clientList = cl;
      clientList = clientList.filter(client => (client.cid === cid));
      const clientsDetails = await this.getClientDetailsOfAll(clientList);
      const response = [];

      clientsDetails.forEach((clientDetails, index) => {
        if (clientDetails !== null) {
          response.push({
            country: clientDetails.client_country,
            time: clientDetails.connection_connected_time,
            cldbid: clientDetails.client_database_id,
            clid: clientList[index].clid,
            name: clientList[index].client_nickname,
          });
        }
      });

      let channelList = await this.qChannelList();
      channelList = channelList.filter(ch => (ch.pid === cid));
      let childClientList = [];

      for (let i = 0; i < channelList.length; i += 1) {
        const child = await this.getClientListOnChannel(channelList[i].cid, cl); // eslint-disable-line
        childClientList = childClientList.concat(child);
      }

      return response.concat(childClientList);
    } catch (error) {
      logger.error(error);
      return [];
    }
  }

  /*
    name: getChannelByName
    description: Zwraca id kanalu za pomocą nazwy
    return: cid || null
  */
  async getChannelByName(name) {
    try {
      let channelList = await this.qChannelFind(name);

      channelList = channelList.filter(
        channel => (channel.channel_name === name),
      );

      if (channelList.length > 0) {
        const cid = channelList[0].cid;
        return cid;
      }
    } catch (error) {
      logger.error(error);
    }

    return null;
  }

  /*
    name: getChannelAdminID
    description: Zwraca id CA
    return: cgid || null
  */
  async getChannelAdminID() {
    try {
      const serverInfo = await this.qServerInfo();
      return serverInfo.virtualserver_default_server_group;
    } catch (error) {
      logger.error(error);
    }

    return null;
  }

  /*
    name: getSlotsInUseByScript
    description: Zwraca ilość slotów używanych przez system
    return: { online, total } || null
  */
  async getSlotsInUseByScript() {
    try {
      const channelList = await this.getChannelList(true);
      const channelsDetails = await this.getChannelDetailsOfAll(channelList);

      channelsDetails.forEach((channelDetails, index) => {
        channelList[index].maxClients = channelDetails.channel_maxfamilyclients;
      });

      let result = {
        online: 0,
        total: 0,
      };

      result = channelList.reduce((prev, curr) => ({
        online: parseInt(curr.clients, 10) + prev.online,
        total: parseInt(curr.maxClients, 10) + prev.total,
      }), result);

      return result;
    } catch (error) {
      logger.error(error);
      return null;
    }
  }

  /*
    name: isEnoughSlots
    description: Sprawdza czy wystarczy slotów po dodaniu kanału
    return: true || false
  */
  async isEnoughSlots(need) {
    try {
      const freeSlots = await this.getFreeSlots();

      if (freeSlots - need >= 0) {
        return true;
      }
    } catch (error) {
      logger.error(error);
    }

    return false;
  }

  /*
    name: getFreeSlots
    description: Zwraca ilość wolnych slotów
    return: int || 0
  */
  async getFreeSlots() {
    try {
      const serverInfo = await this.qServerInfo();
      const clientsOnline = serverInfo.virtualserver_clientsonline;
      const maxClients = serverInfo.virtualserver_maxclients;
      const freeSlots = maxClients - clientsOnline;
      return freeSlots;
    } catch (error) {
      logger.error(error);
    }

    return 0;
  }

  /*
    name: getAdminGroups
    description: Zwraca id grup administracyjnych
    return: array of sgid || []
  */
  async getAdminGroups() {
    try {
      const serverGroupList = await this.qServerGroupList();
      const whitelist = config.ts3.serverAdminGroups;
      const groups = [];

      for (let i = 0; i < serverGroupList.length; i += 1) {
        if (whitelist.indexOf(serverGroupList[i].name) !== -1) {
          groups.push(serverGroupList[i].sgid);
        }
      }

      return groups;
    } catch (error) {
      logger.error(error);
      return [];
    }
  }

  /*
    name: getChannelMemberGroup
    description: Zwraca id grupy użytkownika na kanale
    return: sgid || null
  */
  async getChannelMemberGroup() {
    try {
      const channelGroupList = await this.qChannelGroupList();
      let cgid = null;

      for (let i = 0; i < channelGroupList.length; i += 1) {
        if (channelGroupList[i].name === config.ts3.channelMemberGroup) {
          cgid = channelGroupList[i].cgid;
          break;
        }
      }

      if (cgid === null) {
        throw new Error('Member group not found.');
      }

      return cgid;
    } catch (error) {
      logger.error(error);
      return null;
    }
  }

  /*
    name: getGuestGroup
    description: Zwraca id grupy użytkownika na serwerze
    return: sgid || null
  */
  async getGuestGroup() {
    try {
      const serverGroupList = await this.qServerGroupList();
      let sgid = null;

      for (let i = 0; i < serverGroupList.length; i += 1) {
        if (serverGroupList[i].name === config.ts3.serverGuestGroup) {
          sgid = serverGroupList[i].sgid;
          break;
        }
      }

      if (sgid === null) {
        throw new Error('Guest group not found.');
      }

      return sgid;
    } catch (error) {
      logger.error(error);
      return null;
    }
  }

  /*
    name: getOutdatedChannels
    description: Sprawdza kanały, które powinny zostać usunięte
    return: sgid || []
  */
  async getOutdatedChannels() {
    try {
      const channelList = await this.getChannelList();

      const channels = [];
      if (channelList.length > 0) {
        const channelsDetails = await this.getChannelDetailsOfAll(channelList, true);
        const deleteAfter = config.ts3.delete.cooldownAfterCreate;

        channelsDetails.forEach((channelDetails, index) => {
          if (parseInt(channelDetails.seconds_empty, 10) > deleteAfter) {
            channels.push({
              cid: channelList[index].cid,
              clients: channelList[index].clients,
              channel_name: channelDetails.channel_name,
            });
          }
        });
      }

      return channels;
    } catch (error) {
      logger.error(error);
      return [];
    }
  }

  /*
    name: getClientListOnChannelOfAll
    description: Zwraca listy klientów każdego kanału obsługiwanego przez system
    return: array || []
  */
  async getClientListOnChannelOfAll(cl) {
    try {
      if (cl.length) {
        const clientListOnChannel = [];

        for (let i = 0; i < cl.length; i += 1) {
          clientListOnChannel.push(await this.getClientListOnChannel(cl[i].cid)); // eslint-disable-line
        }

        return clientListOnChannel;
      }
    } catch (error) {
      logger.error(error);
    }

    return [];
  }

  /*
    name: getClientDetailsOfAll
    description: Zwraca listę szczegołów klientów na kanałach obsługiwanych przez system
    return: array || []
  */
  async getClientDetailsOfAll(cl) {
    try {
      if (cl.length) {
        const clientDetails = [];

        for (let i = 0; i < cl.length; i += 1) {
          clientDetails.push(await this.qClientDetails(cl[i].clid)); // eslint-disable-line
        }

        return clientDetails;
      }
    } catch (error) {
      logger.error(error);
    }

    return [];
  }

  /*
    name: getChannelDetailsOfAll
    description: Zwraca listę szczegołów podanych kanałów
    return: array || []
  */
  async getChannelDetailsOfAll(cl, force = false) {
    try {
      if (cl.length) {
        const channelDetails = [];

        for (let i = 0; i < cl.length; i += 1) {
          channelDetails.push(await this.qChannelDetails(cl[i].cid, force)); // eslint-disable-line
        }

        return channelDetails;
      }
    } catch (error) {
      logger.error(error);
    }

    return [];
  }

  /*
    name: getClientDbInfoOfAll
    description: Zwraca listę informacji z db dla każdego klienta
    return: array || []
  */
  async getClientDbInfoOfAll(cl) {
    try {
      if (cl.length) {
        const clientsDbInfo = [];

        for (let i = 0; i < cl.length; i += 1) {
          clientsDbInfo.push(await this.qClientDbInfo(cl[i].cldbid)); // eslint-disable-line
        }

        return clientsDbInfo;
      }
    } catch (error) {
      logger.error(error);
    }

    return [];
  }

  /*
    name: getActiveChannelGroupsOfAll
    description: Zwraca listę użytkowników na kanałach z ich grupami
    return: array || []
  */
  async getActiveChannelGroupsOfAll(cl) {
    try {
      if (cl.length) {
        const activeChannelGroups = [];

        for (let i = 0; i < cl.length; i += 1) {
          activeChannelGroups.push(await this.getActiveChannelGroups(cl[i].cid)); // eslint-disable-line
        }

        return activeChannelGroups;
      }
    } catch (error) {
      logger.error(error);
    }

    return [];
  }

  async createChannel(template) {
    try {
      const channel = await this.qCreateChannel(template.channel);

      if (channel === -2) {
        return -2;
      }

      for (let i = 0; i < template.perms.length; i += 1) {
        this.qChannelAddPerm(
          channel.cid,
          template.perms[i].id,
          template.perms[i].value,
        );
      }

      if (template.sub) {
        template.sub.channel.cpid = channel.cid; // eslint-disable-line no-param-reassign
        this.createChannel(template.sub);
      }
    } catch (error) {
      logger.error(error);
    }
    return null;
  }

  /*
    name: preventLoginToDefault
    description: Zablokowanie logowania się na kanał domyślny
    return: void
  */
  async preventLoginToDefault(data) {
    try {
      if (this.adminGroups === undefined) {
        this.adminGroups = await this.getAdminGroups();
      }

      let groups = data.client_servergroups.split(',');

      groups = groups.filter(
        group => (this.adminGroups.indexOf(group) !== -1),
      );

      if (groups.length === 0) {
        if (this.defaultChannenl === undefined) {
          this.defaultChannel = await this.getDefaultChannelID();
        }

        if (this.defaultChannel === data.ctid) {
          this.qClientKick(data.clid, 5, 'Go away!');
        }
      } else {
        logger.info(`Admin: ${data.client_nickname} zalogował się na serwer.`);
        this.qClientPoke(data.clid, 'Hi my God! What\'s up?');
      }
    } catch (error) {
      logger.error(error);
    }
  }

  async addMemberGroup(ctid, cldbid, cgid) {
    try {
      // jeśli jest guest
      let cid = ctid;
      let pid = ctid;
      for (let i = 0; i < 10; i += 1) { // max 10 childs
        const channelInfo = await this.qChannelDetails(pid); // eslint-disable-line
        cid = pid;
        pid = channelInfo.pid;
        if (parseInt(pid, 10) === 0) {
          break;
        }
      }
      const channelGroupClientList = await this.qChannelGroupClientList(cid);
      if (!channelGroupClientList.find(g => g.cldbid === cldbid)) {
        this.qSetClientChannelGroup(cid, cldbid, cgid);
      }
    } catch (error) {
      logger.error(error);
    }
  }

  /*
    name: onClientEnterView
    description: Uruchamia się w momencie gdy klient wejdzie na serwer
    return: void
  */
  async onClientEnterView(data) {
    try {
      if (data.client_type === 1) {
        return null;
      }

      if (this.guestGroup === undefined) {
        this.guestGroup = await this.getGuestGroup();
      }

      const clientDetails = await this.qClientDetails(data.clid);

      // usuwa grupe zeby mozna bylo wejsc na jakis kanal
      // await this.qServerGroupDelClient(clientDetails.client_database_id, this.guestGroup);
      // wyrzucanie wszystkich z kanalu domyslnego
      await this.preventLoginToDefault(data); // tak musi być i tyle
      // dodawanie grupy zeby uniemozliwic zmiane kanalu
      await this.qServerGroupAddClient(clientDetails.client_database_id, this.guestGroup);

      if (this.channelMemberGroup === undefined) {
        this.channelMemberGroup = await this.getChannelMemberGroup();
      }

      // dodawanie grupy kanalowej zeby moc poruszac sie po subkanalach
      await this.addMemberGroup(
        data.ctid,
        clientDetails.client_database_id,
        this.channelMemberGroup,
      );

      this.cache.insertClientList({
        clid: data.clid,
        cid: data.ctid,
        client_database_id: data.client_database_id,
        client_nickname: data.client_nickname,
        client_type: data.client_type,
      });
    } catch (error) {
      logger.error(error);
    }

    return null;
  }

  async onClientLeftView(data) {
    try {
      this.cache.client.del('clientInfo:'.concat(data.clid));
      this.cache.deleteFromClientList(data.clid);
    } catch (error) {
      logger.error(error);
    }
  }

  async onConnect() {
    try {
      logger.info(`Connected with ${this.config.host}`);
      await this.client.use(this.config.server);
      await this.client.send('servernotifyregister', { event: 'channel', id: 0 });
    } catch (error) {
      logger.error(error);
    }
  }

  async onChannelMoved(data) {
    try {
      this.cache.updateChannelList(data.cid, {
        pid: data.cpid,
        channel_order: data.order,
      });
    } catch (error) {
      logger.error(error);
    }
  }

  /*
    name: onChannelEdited
    description: Uruchamia się w momencie edycji kanału
    return: void
  */
  async onChannelEdited(data) {
    try {
      const x = await this.cache.getAsync('channelInfo:'.concat(data.cid));
      if (x !== null) {
        const temp = {};
        Object.keys(data).forEach((el) => {
          if (el.match(/channel_/g)) {
            temp[el] = data[el];
          }
        });
        Object.assign(x, temp);
        this.cache.set(
          'channelInfo:'.concat(data.cid), x, 'PX', config.cache.channelInfo);
      }

      this.cache.updateChannelList(data.cid, {
        channel_name: data.channel_name,
      });
    } catch (error) {
      logger.error(error);
    }
  }

  async onChannelCreated(data) {
    try {
      this.cache.insertChannelList({
        cid: data.cid,
        pid: data.cpid,
        channel_order: data.channel_order,
        channel_name: data.channel_name,
        total_clients: 0,
        channel_needed_subscribe_power: 0,
      });
    } catch (error) {
      logger.error(error);
    }
  }

  async onChannelDeleted(data) {
    try {
      const channelDetails = await this.qChannelDetails(data.cid);
      if (channelDetails !== null) {
        this.db.removeChannel(channelDetails.channel_name);
      }
      this.cache.client.del('channelInfo:'.concat(data.cid));
      this.cache.client.del('channelGroupClientList:'.concat(data.cid));
      this.cache.deleteFromChannelList(data.cid);
    } catch (error) {
      logger.error(error);
    }
  }

  /*
    name: onClientMoved
    description: Uruchamia się w momencie zmiany kanału przez klienta
    return: void
  */
  async onClientMoved(data) { // eslint-disable-line class-methods-use-this
    try {
      this.cache.updateClientList(data.clid, {
        cid: data.ctid,
      });
    } catch (error) {
      logger.error(error);
    }
  }

  /*
    name: onClose
    description: Uruchamia się w momencie utraty połączenia z serwerem
    return: void
  */
  onClose(err) {
    logger.error(err);
    logger.info(`connection with ${config.ts3.host} lost`);
    logger.info('Reconnection within 5 seconds');
    setTimeout(() => this.connect(), 5 * 1000);
  }

  onError(err) { // eslint-disable-line class-methods-use-this
    logger.error(err);
  }
}

module.exports = InstaTs3;
