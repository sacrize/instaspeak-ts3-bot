const config = {
  start: {
    name: 'bot1',
    port: 3000,
  },
  redis: {
    host: '176.119.62.114',
    ts3: {
      db: 1,
    },
  },
  ts3: {
    host: 'vip1.ts3shield.com',
    port: 10011,
    clientPort: 20763,
    server: 1029,
    disableRegister: true,
    disableUse: true,
    user: 'bot1',
    password: 'HJN6b7lE',
    create: {
      topic: 'TEMPORARY',
      maxCharactersForName: 20,
      template: {
        channel: {
          channel_flag_permanent: 1,
          channel_flag_semi_permanent: 0,
          channel_flag_temporary: 0,
          channel_flag_maxclients_unlimited: 0,
          channel_flag_maxclients_limited: 1,
          channel_flag_maxfamilyclients_unlimited: 0,
          channel_flag_maxfamilyclients_inherited: 0,
          channel_flag_maxfamilyclients_limited: 1,
        },
        // sub: {
        //   channel: {
        //     channel_flag_permanent: 1,
        //     channel_flag_maxclients_unlimited: 1,
        //     channel_flag_maxfamilyclients_inherited: 1,
        //     channel_name: '1',
        //   },
        //   // sub: {},
        //   perms: [
        //     { id: 137, value: 1 }, // i_group_needed_modify_power
        //   ],
        // },
        perms: [
          { id: 137, value: 1 }, // i_group_needed_modify_power
        ],
      },
    },
    delete: {
      cooldownAfterCreate: 10 * 60, // 10 minut w sekundach
    },
    adminPanel: {
      algorithm: 'aes-128-cbc',
      key: 'm#s$s=kGy$6KUL2G',
    },
    serverAdminGroups: ['ROOT'],
    serverGuestGroup: 'Usr',
    channelAdminGroupName: 'CA',
    channelMemberGroup: 'Membr',
  },
  db: {
    host: 'localhost',
    user: 'root',
    // password: '86tp54TADAqjK7wY',
    password: '',
    database: 'instaspeak',
    connectionLimit: 1000,
    connectTimeout: 60 * 60 * 1000,
    aquireTimeout: 60 * 60 * 1000,
    timeout: 60 * 60 * 1000,
  },
  loadBalancer: {
    key: 'hUjEmUj3kUp4WenSzA',
  },
  sendDataToLoadBalancerInterval: 15 * 1000,
  deleteOutdatedChannelsInterval: 20 * 60 * 1000,
  sendChannelsDetailsInterval: 10 * 1000,
  cache: {
    serverInfo: 30 * 1000, // w ms
    channelInfo: 1 * 60 * 60 * 1000, // bedzie czyszczone przez deleteOutdatedChannels co 20min
    clientInfo: 30 * 60 * 1000,
    channelGroupClientList: 1 * 60 * 60 * 1000,
    channelList: 10 * 60 * 1000,
    clientDbInfo: 1 * 60 * 60 * 1000,
    clientList: 10 * 60 * 1000,
    channelGroupList: 24 * 60 * 60 * 1000,
    serverGroupList: 24 * 60 * 60 * 1000,
  },
};

module.exports = config;

// grupa User powinna mieć moc wejscia na kanal 0
// grupa Enter Only (default) moc wejscia na kanal 1
