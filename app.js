const config = require('./config');
const InstaTs3 = require('./ts3');
const InstaDb = require('./db');
const Helmet = require('helmet');
const Crypto = require('crypto');
const Winston = require('winston');

const logger = new Winston.Logger({
  transports: [
    new Winston.transports.Console(),
    new Winston.transports.File({
      name: 'error-file',
      filename: 'errors.log',
      level: 'error',
    }),
  ],
});

const app = require('express')();

app.use(Helmet());
const server = require('http').Server(app);
const io = require('socket.io')(server);

server.listen(config.start.port);

const db = new InstaDb();
const ts3 = new InstaTs3(db);

function decodeAdminPassword(b64) {
  const buf = new Buffer(b64, 'base64');
  const iv = buf.slice(0, 16);
  let pass = buf.slice(16);
  const decipher = Crypto.createDecipheriv(
    config.ts3.adminPanel.algorithm,
    config.ts3.adminPanel.key,
    iv,
  );
  pass = Buffer.concat([
    decipher.update(pass, 'hex'),
    decipher.final(),
  ]).toString('utf8');

  return pass;
}

function isLoadBalancer(socket) {
  const superkey = socket.handshake.headers['x-superkey'];
  return superkey === config.loadBalancer.key;
}

function authLoadBalancer(socket) {
  if (isLoadBalancer(socket)) {
    socket.join('_loadbalancer_');
    socket.emit('welcome', `Hi my god. I am ${config.start.name}`);
  } else {
    logger.error('Unknown request to loadbalancer interface.');
    socket.disconnect();
  }
}

function isValidCode(code) {
  const specialCharacters = /[!@#$%^&*()_+\-=[\]{};':"\\|,.<>/?]/;

  if (code === undefined) {
    logger.info(
      'Próba połączenia bez podania kanału.',
    );
    return false;
  }

  if (specialCharacters.test(code)) {
    logger.info(
      'Znaleziono znaki specjalne w nazwie kanału, rozłączono klienta.',
    );
    return false;
  }

  if (code.length <= 0) {
    logger.info(
      'Długość nazwy kanału <= 0 ?!',
    );
    return false;
  }

  if (code.length > config.ts3.create.maxCharactersForName) {
    logger.info(
      'Przekroczono dozwoloną ilość znaków w nazwie kanału - %d na %d.',
      code.length,
      config.ts3.create.maxCharactersForName,
    );
    return false;
  }

  if (code === '_loadbalancer_') {
    logger.warn(
      'Wykryto próbę połączenia się do kanału loadbalancera.',
    );
    return false;
  }

  return true;
}

function isValidGroup(value) {
  const group = parseInt(value, 10);
  return !isNaN(group) && group > 0;
}

function isValidClient(value) {
  const client = parseInt(value, 10);
  return !isNaN(client) && client > 0;
}

async function joinToRoom(socket, data, admin = false) {
  const address = socket.handshake.address.match(/\d+.\d+.\d+.\d+/g);

  if (!isValidCode(data.code)) {
    socket.disconnect();
    logger.info(
      `[${address}] Podany kod kanału jest błędny.`,
      data.code,
    );
    return false;
  }

  socket.join(data.code);

  if (admin) {
    if (data.pass == null) {
      socket.disconnect();
      logger.warn(
        `[${address}] Klient próbował skorzystać z intefejsu administratora bez podania hasła.`,
      );
      return false;
    }

    const pass = decodeAdminPassword(data.pass);

    try {
      const authenticated = await db.loginToAdminPanel(
        data.code,
        config.ts3.host,
        config.ts3.server,
        pass,
      );

      const room = data.code.concat('_admin');

      if (authenticated) {
        socket.join(room);
        logger.info(
          `[${address}] Klient zalogował się do panelu administracyjnego.`,
          data.code,
        );
      } else {
        socket.disconnect();
        logger.warn(
          `[${address}] Klient próbował zalogować się błędnymi danymi.`,
          room,
          pass,
        );
      }
    } catch (error) {
      logger.error(error);
      return false;
    }
  }

  return true;
}

async function create(socket, data) {
  const address = socket.handshake.address.match(/\d+.\d+.\d+.\d+/g);

  if (!isLoadBalancer(socket)) {
    socket.disconnect();
    logger.warn(
      `[${address}] Wykryto próbę skorzystania z API loadbalancera.`,
    );
  }

  try {
    const isEnoughSlots = await ts3.isEnoughSlots(data.clients);

    if (!isEnoughSlots) {
      throw new Error('Nie wystarczająca ilość slotów.');
    }

    const isChannelFree = await db.isChannelFree(data.code);

    if (!isChannelFree) {
      throw new Error('Kanał istnieje w systemie.');
    }

    const template = config.ts3.create.template;
    Object.assign(template.channel, {
      channel_name: data.code,
      channel_maxclients: data.clients,
      channel_maxfamilyclients: data.clients,
      channel_topic: config.ts3.create.topic,
    });

    const resp = await ts3.createChannel(template);

    if (resp === -2) {
      throw new Error('Znaleziono kanał na serwerze, ale w bazie go nie było.');
    }

    const pass = await db.createChannel(
      data.code,
      config.ts3.host,
      config.ts3.clientPort,
      config.ts3.server,
      address,
    );

    logger.info('Kanał został stworzony pomyślnie.');
    io.to('_loadbalancer_').emit('create', { success: true, id: data.id, pass });
  } catch (errorMsg) {
    logger.error(errorMsg);
    io.to('_loadbalancer_').emit('create', {
      success: false,
      id: data.id,
      error: errorMsg.toString(),
    });
  }
}

async function sendSlotsInUseByScript() {
  try {
    const timer = logger.startTimer(); // benchmark

    const slotsInUseByScript = await ts3.getSlotsInUseByScript();
    io.to('_loadbalancer_').emit('slotsinuse', slotsInUseByScript);

    timer.done('sendSlotsInUseByScript');
  } catch (error) {
    logger.error(error);
  }
}

async function sendServerInfo() {
  try {
    const timer = logger.startTimer(); // benchmark

    const serverInfo = await ts3.qServerInfo();
    io.to('_loadbalancer_').emit('serverinfo', serverInfo);

    timer.done('sendServerInfo');
  } catch (error) {
    logger.error(error);
  }
}

async function deleteOutdatedChannels() {
  try {
    const timer = logger.startTimer(); // benchmark

    const outdatedChannels = await ts3.getOutdatedChannels();

    for (let i = 0; i < outdatedChannels.length; i += 1) {
      // db.removeChannel(outdatedChannels[i].channel_name);
      ts3.qRemoveChannel(outdatedChannels[i].cid);
      io.to(outdatedChannels[i].channel_name).emit('delete');
      logger.info(outdatedChannels[i], 'Channel removed.');
    }

    timer.done('deleteOutdatedChannels');
  } catch (error) {
    logger.error(error);
  }
}

async function sendChannelsDetails() {
  try {
    const timer = logger.startTimer(); // benchmark

    const channelList = await ts3.getChannelList();
    const clientListOnChannels = await ts3.getClientListOnChannelOfAll(channelList);
    const channelsDetails = await ts3.getChannelDetailsOfAll(channelList);
    const activeChannelGroups = await ts3.getActiveChannelGroupsOfAll(channelList);
    const channelGroups = await ts3.qChannelGroupList();

    channelsDetails.forEach((channelDetails, index) => {
      if (channelDetails === null) return;
      const room = channelDetails.channel_name;

      io.to(room).clients((error, clients) => {
        if (error) throw error;

        if (clients.length > 0) {
          io.to(room).emit('is password', channelDetails.channel_flag_password);
          io.to(room).emit('max clients', channelDetails.channel_maxfamilyclients);
          io.to(room).emit('clients', channelList[index].clients);
          io.to(room).emit('clientlist', clientListOnChannels[index]);
        }
      });

      io.to(room.concat('_admin')).clients((error, clients) => {
        if (error) throw error;

        if (clients.length > 0) {
          io.to(room.concat('_admin')).emit('groups', channelGroups);
          io.to(room.concat('_admin')).emit('groups in use', activeChannelGroups[index]);
        }
      });
    }, this);
    timer.done('sendChannelsDetails');
  } catch (error) {
    logger.error(error);
  }
}

async function kick(socket, data) {
  const address = socket.handshake.address.match(/\d+.\d+.\d+.\d+/g);

  if (!isValidCode(data.code)) {
    logger.warn(address, 'Błędny kod kanału.', data.code);
    socket.emit('err', 'This code is not valid.');
    return false;
  }

  if (!isValidClient(data.client)) {
    logger.warn(address, 'Błędna wartość clid', data.client);
    socket.emit('err', 'This client id is not valid.');
    return false;
  }

  if (data.code.concat('_admin') in socket.rooms === false) {
    socket.disconnect();
    logger.warn(
      `[${address}] Wykryto próbę wywołania metody administracyjnej przez nieuprawnionego klienta.`,
    );
    return false;
  }

  try {
    ts3.qClientKick(data.client, 5, 'You were kicked by channel admin.');
    return true;
  } catch (error) {
    logger.error(error);
    return false;
  }
}

async function changeGroup(socket, data) {
  const address = socket.handshake.address.match(/\d+.\d+.\d+.\d+/g);

  if (!isValidCode(data.code)) {
    logger.warn(`[${address}] Błędny kod kanału.`, data.code);
    socket.emit('err', 'This code is not valid.');
    return false;
  }

  if (!isValidClient(data.client)) {
    logger.warn(`[${address}] Błędna wartość clid`, data.client);
    socket.emit('err', 'This client id is not valid.');
    return false;
  }

  if (!isValidGroup(data.client)) {
    logger.warn(`[${address}] Błędna wartość cgid`, data.client);
    socket.emit('err', 'This group id is not valid.');
    return false;
  }

  if (data.code.concat('_admin') in socket.rooms === false) {
    socket.disconnect();
    logger.warn(
      address,
      'Wykryto próbę wywołania metody administracyjnej przez nieuprawnionego klienta.',
    );
    return false;
  }

  try {
    const cid = await ts3.getChannelByName(data.code);

    if (cid === -1) {
      logger.warn(address, 'Nie znaleziono podanego kanału na serwerze.', data.code);
      socket.emit('err', 'The specified channel is not exists.');
      return false;
    }

    await ts3.qSetClientChannelGroup(cid, data.client, data.group);
    socket.emit('succ', 'Group has been changed');
    return true;
  } catch (error) {
    logger.error(error);
    socket.emit('err', 'The specified channel is not exists.');
    return false;
  }
}

setInterval(() => deleteOutdatedChannels(), config.deleteOutdatedChannelsInterval);
setInterval(() => sendChannelsDetails(), config.sendChannelsDetailsInterval);

setInterval(() => {
  sendServerInfo(); // to loadbalancer
  sendSlotsInUseByScript(); // to loadbalancer
}, config.sendDataToLoadBalancerInterval);


io.on('connection', (socket) => {
  socket.on('room', room => joinToRoom(socket, room));
  socket.on('admin', data => joinToRoom(socket, data, true));
  socket.on('changeGroup', data => changeGroup(socket, data));
  socket.on('kick', data => kick(socket, data));
  socket.on('create', data => create(socket, data));
  socket.on('loadbalancer login', () => authLoadBalancer(socket));
});
