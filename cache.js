const config = require('./config');
const Redis = require('redis');
const Bluebird = require('bluebird');
const Winston = require('winston');

Bluebird.promisifyAll(Redis.RedisClient.prototype);

const logger = new Winston.Logger({
  transports: [
    new Winston.transports.Console(),
    new Winston.transports.File({
      name: 'error-file',
      filename: 'errors.log',
      level: 'error',
    }),
  ],
});

class Cache {
  constructor() {
    this.client = Redis.createClient(config.redis);
    this.client.select(config.redis.ts3.db);
    this.client.flushdb();
    this.client.on('error', this.onError);
    this.client.on('reconnecting', this.onReconnecting);
    this.client.on('connect', this.onConnect);
  }

  async set(key, value, mode, time) {
    try {
      value = JSON.stringify(value); // eslint-disable-line no-param-reassign
      this.client.set(key, value, mode, time);
    } catch (error) {
      logger.error(error);
    }
  }

  async getAsync(key) {
    try {
      const x = await this.client.getAsync(key);
      return JSON.parse(x);
    } catch (error) {
      logger.error(error);
      return null;
    }
  }

  async insertClientList(data) {
    try {
      const x = await this.getAsync('clientList');
      if (x !== null) {
        x.forEach((cl) => {
          if (cl.clid === data.clid) {
            throw new Error(`Próba dodania do cache klienta, który już tam istnieje: ${cl.clid}`);
          }
        });
        x.push(data);
        this.set(
          'clientList', x, 'PX', config.cache.clientList);
      }
    } catch (error) {
      logger.error(error);
    }
  }

  async deleteFromClientList(clid) {
    try {
      const x = await this.getAsync('clientList');
      if (x !== null) {
        const i = x.findIndex(cl => cl.clid === clid);
        x.splice(i, 1);
        if (x.length) {
          this.set(
            'clientList', x, 'PX', config.cache.clientList);
        } else {
          this.client.del('clientList');
        }
      }
    } catch (error) {
      logger.error(error);
    }
  }

  async updateClientList(clid, data) {
    try {
      const x = await this.getAsync('clientList');
      if (x !== null) {
        for (let i = 0; i < x.length; i += 1) {
          if (x[i].clid === clid) {
            Object.assign(x[i], data);
            break;
          }
        }
        this.set(
          'clientList', x, 'PX', config.cache.clientList);
      }
    } catch (error) {
      logger.error(error);
    }
  }

  async insertChannelList(data) {
    try {
      const x = await this.getAsync('channelList');
      if (x !== null) {
        x.forEach((ch) => {
          if (ch.cid === data.cid) {
            throw new Error(`Próba dodania do cache kanału, który już tam istnieje: ${ch.cid}`);
          }
        });
        x.push(data);
        this.set(
          'channelList', x, 'PX', config.cache.channelList);
      }
    } catch (error) {
      logger.error(error);
    }
  }

  async deleteFromChannelList(cid) {
    try {
      const x = await this.getAsync('channelList');
      if (x !== null) {
        const i = x.findIndex(ch => ch.cid === cid);
        x.splice(i, 1);
        if (x.length) {
          this.set(
            'channelList', x, 'PX', config.cache.channelList);
        } else {
          this.client.del('channelList');
        }
      }
    } catch (error) {
      logger.error(error);
    }
  }

  async updateChannelList(cid, data) {
    try {
      const x = await this.getAsync('channelList');
      if (x !== null) {
        for (let i = 0; i < x.length; i += 1) {
          if (x[i].cid === cid) {
            Object.assign(x[i], data);
            break;
          }
        }
        this.set(
          'channelList', x, 'PX', config.cache.channelList);
      }
    } catch (error) {
      logger.error(error);
    }
  }

  onError(error) { // eslint-disable-line
    logger.error(error);
  }

  onReconnecting(msg) { // eslint-disable-line
    logger.info('Nawiązywanie połączenia z cache..');
  }

  onConnect() { // eslint-disable-line
    logger.info('Połączono z cache.');
  }
}

module.exports = Cache;
