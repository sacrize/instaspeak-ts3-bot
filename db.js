const config = require('./config');
const Mysql = require('mysql');
const Crypto = require('crypto');
const Winston = require('winston');

const logger = new Winston.Logger({
  transports: [
    new Winston.transports.Console(),
    new Winston.transports.File({
      name: 'error-file',
      filename: 'errors.log',
      level: 'error',
    }),
  ],
});

class InstaDb {
  constructor() {
    this.unique = 0;
    this.pool = Mysql.createPool(config.db);
  }

  isChannelFree(code) {
    return new Promise((resolve, reject) => {
      this.pool.getConnection((connectionError, connectionLink) => {
        if (connectionError) {
          reject(connectionError);
        }

        let sql = 'SELECT count(1) as count FROM links WHERE code=';
        sql += connectionLink.escape(code);

        connectionLink.query(sql, (error, results) => {
          connectionLink.release();

          if (error) {
            reject(error);
          }

          const isFree = results[0].count < 1;

          if (isFree) {
            resolve(true);
          } else {
            resolve(false);
          }
        });
      });
    }).catch(logger.error);
  }

  loginToAdminPanel(code, host, vserv, pass) {
    return new Promise((resolve, reject) => {
      this.pool.getConnection((connectionError, connectionLink) => {
        if (connectionError) {
          reject(connectionError);
        }

        let sql = 'SELECT 1 FROM links WHERE code=';
        sql += connectionLink.escape(code);
        sql += ' AND password=';
        sql += connectionLink.escape(pass);
        sql += ' AND host=';
        sql += connectionLink.escape(host);
        sql += ' AND vserv=';
        sql += connectionLink.escape(vserv);

        connectionLink.query(sql, (error, results) => {
          connectionLink.release();

          if (error) {
            reject(error);
          }

          const canLogin = results.length > 0;

          if (canLogin) {
            resolve(true);
          } else {
            resolve(false);
          }
        });
      });
    }).catch(logger.error);
  }

  createChannel(code, host, port, server, ip) {
    return new Promise((resolve, reject) => {
      this.pool.getConnection((connectionError, connectionLink) => {
        if (connectionError) {
          reject(connectionError);
        }

        const pass = this.makeUniquePassword(code);

        let sql = 'INSERT INTO links SET code=';
        sql += connectionLink.escape(code);
        sql += ', password=PASSWORD(\'';
        sql += pass;
        sql += '\'), vserv=';
        sql += connectionLink.escape(server);
        sql += ', port=';
        sql += connectionLink.escape(port);
        sql += ', host=';
        sql += connectionLink.escape(host);
        sql += ', clientip=';
        sql += connectionLink.escape(ip);

        connectionLink.query(sql, (error) => {
          connectionLink.release();

          if (error) {
            reject(error);
          }

          const response = {
            code,
            pass,
          };

          resolve(response);
        });
      });
    }).catch(logger.error);
  }

  removeChannel(code) {
    return new Promise((resolve, reject) => {
      this.pool.getConnection((connectionError, connectionLink) => {
        if (connectionError) {
          reject(connectionError);
        }

        let sql = 'DELETE FROM links WHERE code=';
        sql += connectionLink.escape(code);

        connectionLink.query(sql, (error) => {
          connectionLink.release();

          if (error) {
            reject(error);
          }

          resolve();
        });
      });
    }).catch(logger.error);
  }

  makeUniquePassword(code) {
    this.unique += 1;
    const seed = code + Date.now() + this.unique;
    const password = Crypto.createHmac('sha256', seed).digest('hex').substring(1, 10);
    return password;
  }
}

module.exports = InstaDb;
